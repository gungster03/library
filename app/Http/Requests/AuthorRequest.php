<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author' => "required|min:2|max:250",
        ];
    }


    public function messages()
    {
        return [
            'author.required' => 'The name is required.',
            'author.min' => 'The name should be more than 2.',
            'author.max' => 'The name should be less than 250.',
        ];
    }
}
