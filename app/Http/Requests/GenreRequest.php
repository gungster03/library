<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'genre' => 'required|max:250|min:10'
        ];
    }

    public function messages()
    {
        return [
            'genre.required' => 'The title is required.',
            'genre.max' => 'The title should be less than 250 symbols.',
            'genre.min' => 'The title should be more than 10 symbols .',
        ];
    }
}
