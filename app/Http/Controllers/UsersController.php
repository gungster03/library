<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends AuthController
{

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function register(Request $request)
    {
        If($request->session()->has('user_id')){
            return redirect()->route('home')->with('error','u are already registered!');
        }
        return view('users.register');
    }

    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request){
        $payload = $request->validated();
        $payload['password'] = Hash::make($payload['password']);
        $user = User::create($payload);
        $user->logIn($user);
        return redirect()->route('users.ticket')->with('success','You are registered successfully');
    }
}
