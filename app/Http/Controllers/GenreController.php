<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenreRequest;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $genres = Genre::all();
        return view('genres.index', compact('genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('genres.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param GenreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(GenreRequest $request): RedirectResponse
    {
        $genre = new Genre();
        $genre->genre = $request->input('genre');
        $genre->save();
        return redirect()->route('genres.index')->with('status', "Genre: {$genre->genre} successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Genre $genre
     * @return Application|Factory|View|Response
     */
    public function show(Genre $genre)
    {
        $genre->setRelation('books', $genre->books()->simplePaginate(9));

        return view('genres.show', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Genre $genre
     * @return Application|Factory|View|Response
     */
    public function edit(Genre $genre)
    {
        return view('genres.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Genre $genre
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(GenreRequest $request, Genre $genre): RedirectResponse
    {
        $genre->genre = $request->input('genre');
        $genre->save();
        return redirect()->route('genres.index')->with('status', "Genre: {$genre->genre} successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Genre $genre
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Genre $genre)
    {
        $genre->delete();
        return redirect()->route('genres.index')->with('status', "Genre: {$genre->genre} successfully deleted");
    }
}
