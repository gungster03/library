<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class BookController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(){
        $books = Book::paginate(12);
        return view('books.index', compact('books'));
    }


    /**
     * @param Book $book
     * @return Application|Factory|View
     */
    public function show(Book $book)
    {
        return view('books.show', compact('book'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $authors = Author::all();
        $genres = Genre::all();
        return view('books.create', compact('genres','authors'));
    }
    /**
     * @param Book $book
     * @return Application|Factory|View
     */
    public function edit(Book $book)
    {
        $genres = Genre::all();
        $authors = Author::all();
        return view('books.edit', compact('book','genres','authors'));
    }


    /**
     * @param BookRequest $request
     * @param Book $book
     * @return RedirectResponse
     */
    public function update(BookRequest $request, Book $book):RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $book->update($data);
        return redirect()->action([self::class, 'show'], ['book' => $book])->with('status', "book {$book->title} successfully updated!");
    }

    /**
     * @param BookRequest $request
     * @return RedirectResponse
     */
    public function store(BookRequest $request):RedirectResponse
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $book = new Book($data);
        $book->save();
        return redirect(route('books.index'))->with('success', "Book: {$book->title} added successfully");
    }


    /**
     * @param Book $book
     * @return RedirectResponse
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index')->with('status', "{$book->name} successfully deleted!");
    }
}
