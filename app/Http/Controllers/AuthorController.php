<?php

namespace App\Http\Controllers;


use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class AuthorController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $authors = Author::all();
        return view('authors.index', compact('authors'));
    }

}
