@extends('layouts.app')
@section('content')
    <div><h1>All Books</h1></div>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($books as $book)
            <div class="col" style="margin-bottom:10px">
                <div class="card h-100">
                    @if($book->picture)
                        <img src="{{asset('/storage/' .$book->picture)}}" class="card-img-top" alt="{{$book->picture}}" width="100" height="300">
                    @else
                        <img src="{{asset('/storage/pictures/img.png')}}" class="card-img-top" alt="..." width="100" height="300">
                    @endif
                    <div class="card-body">
                        <a href="{{route('books.show', ['book' => $book])}}" ><h5 class="card-title">{{$book->name}}</h5></a>
                        <h5 class="card-title">{{$book->title}}</h5>
                        <p class="card-text">{{is_null($book->genre) ? "No genre": $book->genre->genre}}</p>
                        <p class="card-text">Автор:{{is_null($book->author) ? "No author": $book->author->author}}</p>
                        <a href="{{route('books.show',['book' => $book])}}">Подробнее...</a>
{{--                        <a href="{{route('')}}"></a>--}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row">
        <div class="vol-md-auto">
            {{$books->links('pagination::bootstrap-4')}}
        </div>
    </div>
@endsection
