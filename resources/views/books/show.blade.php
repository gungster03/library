@extends('layouts.app')
@section('content')
    @if($book->picture)
        <img src="{{asset('/storage/' .$book->picture)}}" class="img-thumbnail" alt="{{$book->picture}}"  width="400" height="1000">
    @else
        <img src="{{asset('/storage/pictures/img.png')}}" class="rounded float-start" alt="..." width="200" height="300">
    @endif
    <h3>{{$book->title}}</h3>
    <h5>Описание: {{$book->description}}</h5>
    <h5>Жанр: <p class="card-text">{{is_null($book->genre) ? "No genre": $book->genre->genre}}</p>
    </h5>
    <h5>Автор:  <p class="card-text">{{is_null($book->author) ? "No author": $book->author->author}}</p>
    </h5>


@endsection

