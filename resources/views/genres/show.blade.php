@extends('layouts.app')
@section('content')
    <div style="margin-left:25%">
       <div class="d-flex align-items-center mt-4">
        <div class="flex-grow-1 ms-3">
            <h3>Книги жанра {{$genre->genre}}</h3>
        </div>
    </div>
    </div>
    <div class="row row-cols-md-3 g-4">
    @foreach($genre->books as $book)

            <div class="col" style="margin-bottom: 10px">
                <div class="card h-100">
                    @if($book->picture)
                        <img src="{{asset('/storage/' .$book->picture)}}" class="card-img-top" alt="{{$book->picture}}" width="100" height="400">
                    @else
                        <img src="{{asset('/storage/pictures/img.png')}}" class="card-img-top" alt="..." width="200" height="300">
                    @endif
                    <div class="card-body">
                        <a href="{{route('books.show', ['book' => $book])}}" ><h5 class="card-title">{{$book->name}}</h5></a>
                        <h5 class="card-title">{{$book->title}}</h5>
                        <p class="card-text">{{is_null($book->genre) ? "No genre": $book->genre->genre}}</p>
                        <p class="card-text">Автор:{{is_null($book->author) ? "No author": $book->author->author}}</p>
                        <a href="{{route('books.show',['book' => $book])}}">Подробнее...</a>
                    </div>
                </div>
            </div>

    @endforeach
    </div>
    <div class="row">
        <div class="vol-md-auto">
                {{$genre->books->links()}}
        </div>
    </div>
@endsection
