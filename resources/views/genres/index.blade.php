@extends('layouts.app')
 @section('content')
     <div class="w3-sidebar w3-light-grey w3-bar-block" style="width:25%">
         <h3 class="w3-bar-item">Menu</h3>
         <a href="{{route('books.index')}}">all</a>
         @foreach($genres as $genre)
             <a href="{{route('genres.show', ['genre' => $genre])}}">{{$genre->genre}}</a>
         @endforeach
     </div>
 @endsection
