<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\GenreController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GenreController::class, 'index'])->name('home');
Route::resource('books', BookController::class);
Route::resource('genres', GenreController::class);
Route::resource('authors', AuthorController::class);
Route::get('/users/register', [App\Http\Controllers\UsersController::class, 'register'])->name('users.register');
Route::post('/users', [App\Http\Controllers\UsersController::class, 'store'])->name('users.store');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
